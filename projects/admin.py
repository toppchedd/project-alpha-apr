from django.contrib import admin
from .models import Project


# add to admin page at localhost8000
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "owner",
    ]
