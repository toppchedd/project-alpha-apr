from django.forms import ModelForm
from projects.models import Project


# form for project creation
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
