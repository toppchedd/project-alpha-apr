from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from .models import Task


# create a task logged in user
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


# show tasks of logged in user
@login_required
def show_my_tasks(request):
    mytasks = Task.objects.filter(assignee=request.user)
    context = {
        "mytasks": mytasks,
    }
    return render(request, "tasks/mytasks.html", context)
