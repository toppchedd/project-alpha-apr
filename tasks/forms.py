from django.forms import ModelForm, DateInput
from .models import Task


# creates calendar in form window to select date
class DateInput(DateInput):
    input_type = "date"


# shows form fields to create task
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {"start_date": DateInput(), "due_date": DateInput()}
