from django.urls import path
from .views import create_task, show_my_tasks


# paths to navigate 
urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
